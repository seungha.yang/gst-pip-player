# 1. Prerequisites Setup

- Install git and python 3.6+
- Install meson and ninja
  - Install `meson` via Python's `pip` tool. This will also install `ninja`.
- GStreamer 1.18 or newer development package

# 2. Build

``` sh
# (Windows only) add GStreamer to PATH
$env:PATH += ";C:\gstreamer\1.0\msvc_x86_64\bin"

# Setup the builddir for the project
meson builddir

# Compile the project
meson compile -C builddir
```

# 3. Run binary

We will use Meson's devenv feature to setup the necessary environment variables automatically

```sh
# (Windows only) Like before, we need to add gstreamer to PATH
$env:PATH += ";C:\gstreamer\1.0\msvc_x86_64\bin"

# Enter the development environment, which has all the env vars set for us already
meson devenv -C builddir

# Available options
(gst-pip-player) pip-player --help
Usage:
  pip-player.exe [OPTION…] FILE1|URI1 [FILE2|URI2] [FILE3|URI3] ...

Help Options:
  -h, --help                        Show help options
  --help-all                        Show all help options
  --help-gst                        Show GStreamer Options

Application Options:
  --scale-factor                    Scale factor for secondary stream, in [0.0, 1.0] range
  --show-plugins                    Print list of plugins for decoding and composing and exit
  --video-decoder                   Specify video decoder element to use
  --video-compositor                Specify video compositor element to use
  --video-sink                      Specify video sink element to use
  --video-sink-caps                 Specify caps for video sink element to be negotiated (example: "video/x-raw,format=RGBx")
  --benchmark                       Enables benchmark for forward playback


# Check available video decoder and compositor plugins
(gst-pip-player) pip-player --show-plugins
Checking available hardware decoders...

[Found "2" D3D11 H.264 Decoder(s)]
d3d11h264dec: Direct3D11/DXVA H.264 AMD Radeon(TM) Graphics Decoder
d3d11h264device1dec: Direct3D11/DXVA H.264 NVIDIA GeForce RTX 3060 Laptop GPU Decoder

[Found "2" NVDEC H.264 Decoder(s)]
nvh264dec: NVDEC h264 Video Decoder
nvh264sldec: NVDEC H.264 Stateless Decoder

[Found LIBAV H.264 Decoder]
avdec_h264: libav H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10 decoder

Checking available composing elements...

[Found "3" Compositor element(s)]
d3d11compositor: Direct3D11 Compositor Bin
glvideomixer: OpenGL video_mixer bin
compositor: Compositor
```

# 4. Example

## Playout using Direct3D11 decoder and Direct3D11 compositor

```sh
(gst-pip-player) pip-player \
  --video-decoder=d3d11h264dec \
  --video-compositor=d3d11compositor \
  primary_file.mp4 \
  secondary_file.mp4
```

## Measure performance with custom videosink element
```sh
(gst-pip-player) pip-player \
  --benchmark \
  --video-sink=my-custom-sink \
  --video-sink-caps="video/x-raw,format=RGBx" \
  primary_file.mp4 \
  secondary_file.mp4
```

