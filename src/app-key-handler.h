#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

typedef enum
{
  KB_ARROW_UP = 0,
  KB_ARROW_DOWN,
  KB_ARROW_LEFT,
  KB_ARROW_RIGHT,
} VirtualKeyValue;

typedef void (*KeyInputCallback) (gchar input, gboolean is_ascii, gpointer user_data);

void set_key_handler (GMainContext * context, KeyInputCallback callback, gpointer user_data);

void unset_key_handler (void);

G_END_DECLS
