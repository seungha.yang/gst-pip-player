#include "gstpipplayer.h"
#include <gst/pbutils/pbutils.h>

GST_DEBUG_CATEGORY_STATIC (gst_pip_player_debug);
#define GST_CAT_DEFAULT gst_pip_player_debug

typedef void (*ThreadFunc) (gpointer data);
typedef struct
{
  GstPipPlayer *self;
  ThreadFunc func;
  gpointer data;
  gboolean fired;
} ThreadData;

struct _GstPipPlayer
{
  GstObject parent;

  GThread *thread;
  GMainContext *context;
  GMainLoop *loop;
  GstDiscoverer *discoverer;

  GMutex lock;
  GCond cond;

  GRecMutex api_lock;

  GstState current_state;
  GstState target_state;

  GstStructure *config;
  gchar *primary_uri;
  gchar *secondary_uri;

  GstDiscovererInfo *primary_info;
  GstDiscovererInfo *secondary_info;

  GstElement *pipeline;

  GstPad *primary_pad;
  GstPad *secondary_pad;

  gboolean saw_primary;
  gboolean saw_secondary;

  GstEvent *pending_seek;

  GstClockTime segment_start;
  GstClockTime segment_stop;
  GstClockTime duration;

  GstClockTime secondary_segment_start;
  GstClockTime secondary_segment_stop;

  gdouble rate;
  guint seek_flags;

  gboolean got_eos;
  gint64 last_position;

  GstBus *app_bus;
};

struct _GstPipPlayerClass
{
  GstObjectClass parent_class;
};

static void gst_pip_player_constructed (GObject * object);
static void gst_pip_player_dispose (GObject * object);
static void gst_pip_player_finalize (GObject * object);
static gpointer gst_pip_player_thread_func (GstPipPlayer * self);
static void gst_pip_player_reset (GstPipPlayer * self);

static gboolean gst_pip_player_create_pipeline (GstPipPlayer * self);

#define gst_pip_player_parent_class parent_class
G_DEFINE_TYPE (GstPipPlayer, gst_pip_player, GST_TYPE_OBJECT);

static void
gst_pip_player_class_init (GstPipPlayerClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = gst_pip_player_constructed;
  object_class->dispose = gst_pip_player_dispose;
  object_class->finalize = gst_pip_player_finalize;

  GST_DEBUG_CATEGORY_INIT (gst_pip_player_debug, "pip-player", 0, "pip-player");
}

static void
gst_pip_player_init (GstPipPlayer * self)
{
  g_mutex_init (&self->lock);
  g_cond_init (&self->cond);
  g_rec_mutex_init (&self->api_lock);

  self->context = g_main_context_new ();
  self->loop = g_main_loop_new (self->context, FALSE);
  self->config = gst_structure_new_empty ("GstPipPlayerConfig");
  self->target_state = GST_STATE_NULL;
  self->rate = 1.0;

  gst_pip_player_reset (self);
}

static void
gst_pip_player_constructed (GObject * object)
{
  GstPipPlayer *self = GST_PIP_PLAYER (object);

  g_mutex_lock (&self->lock);
  self->thread =
      g_thread_new ("pip-player", (GThreadFunc) gst_pip_player_thread_func,
      self);
  while (!g_main_loop_is_running (self->loop))
    g_cond_wait (&self->cond, &self->lock);
  g_mutex_unlock (&self->lock);

  G_OBJECT_CLASS (parent_class)->constructed (object);
}

static void
gst_pip_player_dispose (GObject * object)
{
  GstPipPlayer *self = GST_PIP_PLAYER (object);

  gst_clear_object (&self->app_bus);

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_pip_player_finalize (GObject * object)
{
  GstPipPlayer *self = GST_PIP_PLAYER (object);

  g_main_loop_quit (self->loop);
  if (self->thread != g_thread_self ())
    g_thread_join (self->thread);
  g_main_loop_unref (self->loop);
  g_main_context_unref (self->context);

  g_mutex_clear (&self->lock);
  g_cond_clear (&self->cond);
  g_rec_mutex_clear (&self->api_lock);
  g_clear_pointer (&self->config, gst_structure_free);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gst_pip_player_loop_running_cb (GstPipPlayer * self)
{
  GST_INFO_OBJECT (self, "Loop running");

  g_mutex_lock (&self->lock);
  g_cond_signal (&self->cond);
  g_mutex_unlock (&self->lock);

  return G_SOURCE_REMOVE;
}

static void
gst_pip_player_reset (GstPipPlayer * self)
{
  if (self->pipeline) {
    GstBus *bus = gst_element_get_bus (self->pipeline);
    gst_bus_remove_watch (bus);
    gst_object_unref (bus);

    gst_element_set_state (self->pipeline, GST_STATE_NULL);
    gst_clear_object (&self->primary_pad);
    gst_clear_object (&self->secondary_pad);
    gst_clear_object (&self->pipeline);
  }

  g_clear_pointer (&self->primary_uri, g_free);
  g_clear_pointer (&self->secondary_uri, g_free);

  g_clear_pointer (&self->primary_info, g_object_unref);
  g_clear_pointer (&self->secondary_info, g_object_unref);

  gst_clear_event (&self->pending_seek);

  self->saw_primary = FALSE;
  self->saw_secondary = FALSE;
  self->duration = GST_CLOCK_TIME_NONE;
  self->current_state = GST_STATE_NULL;
  self->got_eos = FALSE;
}

static gpointer
gst_pip_player_thread_func (GstPipPlayer * self)
{
  GSource *source;

  g_main_context_push_thread_default (self->context);

  source = g_idle_source_new ();
  g_source_set_callback (source, (GSourceFunc) gst_pip_player_loop_running_cb,
      self, nullptr);
  g_source_attach (source, self->context);
  g_source_unref (source);
  self->discoverer = gst_discoverer_new (10 * GST_SECOND, nullptr);

  GST_DEBUG_OBJECT (self, "Enter loop");
  g_main_loop_run (self->loop);
  GST_DEBUG_OBJECT (self, "Quit loop");

  gst_pip_player_reset (self);
  gst_clear_object (&self->discoverer);

  g_main_context_pop_thread_default (self->context);

  return nullptr;
}

#define DEFAULT_OPT_VIDEO_DECODER "avdec_h264"
#define DEFAULT_OPT_VIDEO_COMPOSITOR "compositor"
#define DEFAULT_OPT_VIDEO_SINK "autovideosink"
#define DEFAULT_OPT_VIDEO_SINK_CAPS_STR "video/x-raw(ANY)"
#define DEFAULT_OPT_SCALE_FACTOR 0.4
#define DEFAULT_OPT_SYNC TRUE
#define DEFAULT_OPT_TIMESTAMP_OFFSET G_GINT64_CONSTANT(0)
#define DEFAULT_OPT_SEGMENT_START G_GINT64_CONSTANT(0)
#define DEFAULT_OPT_SEGMENT_STOP G_GINT64_CONSTANT(-1)

static const gchar *
get_string_opt (GstPipPlayer * self, const gchar * opt,
    const gchar * default_val)
{
  const gchar *ret;

  ret = gst_structure_get_string (self->config, opt);
  if (!ret)
    ret = default_val;

  return ret;
}

static gdouble
get_double_opt (GstPipPlayer * self,
    const gchar * opt, gdouble min, gdouble max, gdouble default_val)
{
  gdouble ret;

  if (gst_structure_get_double (self->config, opt, &ret)) {
    if (ret < min || ret > max)
      ret = default_val;
  } else {
    ret = default_val;
  }

  return ret;
}

static gboolean
get_boolean_opt (GstPipPlayer * self, const gchar * opt, gboolean default_val)
{
  gboolean ret;

  if (!gst_structure_get_boolean (self->config, opt, &ret))
    ret = default_val;

  return ret;
}

static gint64
get_int64_opt (GstPipPlayer * self, const gchar * opt, gint64 default_val)
{
  gint64 ret;

  if (gst_structure_get_int64 (self->config, opt, &ret))
    return ret;

  return default_val;
}

#define GET_OPT_VIDEO_DECODER(p)                                               \
  get_string_opt(p, GST_PIP_PLAYER_VIDEO_DECODER, DEFAULT_OPT_VIDEO_DECODER)
#define GET_OPT_VIDEO_COMPOSITOR(p)                                            \
  get_string_opt(                                                              \
    p, GST_PIP_PLAYER_VIDEO_COMPOSITOR, DEFAULT_OPT_VIDEO_COMPOSITOR)
#define GET_OPT_VIDEO_SINK(p)                                                  \
  get_string_opt(p, GST_PIP_PLAYER_VIDEO_SINK, DEFAULT_OPT_VIDEO_SINK)
#define GET_OPT_VIDEO_SINK_CAPS_STR(p)                                         \
  get_string_opt(                                                              \
    p, GST_PIP_PLAYER_VIDEO_SINK_CAPS_STR, DEFAULT_OPT_VIDEO_SINK_CAPS_STR)
#define GET_OPT_SCALE_FACTOR(p)                                                \
  get_double_opt(p,                                                            \
                 GST_PIP_PLAYER_OVERLAY_SCALE_FACTOR,                          \
                 0.0,                                                          \
                 1.0,                                                          \
                 DEFAULT_OPT_SCALE_FACTOR)
#define GET_OPT_SYNC(p)                                                        \
  get_boolean_opt(p, GST_PIP_PLAYER_SYNC, DEFAULT_OPT_SYNC)
#define GET_OPT_TIMESTMAP_OFFSET(p)                                            \
  get_int64_opt(                                                               \
    p, GST_PIP_PLAYER_TIMESTAMP_OFFSET, DEFAULT_OPT_TIMESTAMP_OFFSET)
#define GET_OPT_SEGMENT_START(p)                                               \
  get_int64_opt(p, GST_PIP_PLAYER_SEGMENT_START, DEFAULT_OPT_SEGMENT_START)
#define GET_OPT_SEGMENT_STOP(p)                                                \
  get_int64_opt(p, GST_PIP_PLAYER_SEGMENT_STOP, DEFAULT_OPT_SEGMENT_STOP)

static gboolean
run_sync_source_func (ThreadData * data)
{
  GstPipPlayer *self = data->self;

  data->func (data->data);

  g_mutex_lock (&self->lock);
  data->fired = TRUE;
  g_cond_broadcast (&self->cond);
  g_mutex_unlock (&self->lock);

  return G_SOURCE_REMOVE;
}

static void
run_sync (GstPipPlayer * self, ThreadFunc func, gpointer data)
{
  ThreadData tdata;

  tdata.self = self;
  tdata.func = func;
  tdata.data = data;
  tdata.fired = FALSE;

  g_main_context_invoke (self->context, (GSourceFunc) run_sync_source_func,
      &tdata);

  g_mutex_lock (&self->lock);
  while (!tdata.fired)
    g_cond_wait (&self->cond, &self->lock);
  g_mutex_unlock (&self->lock);
}

typedef struct
{
  GstPipPlayer *self;
  const gchar *primary_uri;
  const gchar *secondary_uri;
  GError **err;
  gboolean ret;
} SetUrisData;

static GstDiscovererInfo *
validate_uri (GstPipPlayer * self, const gchar * uri, GError ** err)
{
  GstDiscovererInfo *info;
  GList *streams;
  GstClockTime duration;

  info = gst_discoverer_discover_uri (self->discoverer, uri, err);
  if (!info) {
    GST_WARNING_OBJECT (self, "Failed to discover URI %s", uri);
    return nullptr;
  }

  duration = gst_discoverer_info_get_duration (info);
  if (!GST_CLOCK_TIME_IS_VALID (duration)) {
    gst_discoverer_info_unref (info);
    GST_WARNING_OBJECT (self, "Unknown duration for URI %s", uri);
    return nullptr;
  }

  streams = gst_discoverer_info_get_video_streams (info);
  if (!streams) {
    GST_WARNING_OBJECT (self, "No video stream in uri stream %s", uri);
    gst_discoverer_info_unref (info);
    g_set_error (err,
        GST_STREAM_ERROR,
        GST_STREAM_ERROR_TYPE_NOT_FOUND,
        "URI %s doesn't contain video stream", uri);
    return nullptr;
  }

  gst_discoverer_stream_info_list_free (streams);

  return info;
}

static void
setup_pipeline (SetUrisData * data)
{
  GstPipPlayer *self = data->self;
  GstDiscovererInfo *primary_info = nullptr;
  GstDiscovererInfo *secondary_info = nullptr;

  data->ret = FALSE;

  primary_info = validate_uri (self, data->primary_uri, data->err);
  if (!primary_info)
    return;

  if (data->secondary_uri) {
    secondary_info = validate_uri (self, data->secondary_uri, data->err);
    if (!secondary_info) {
      gst_discoverer_info_unref (primary_info);
      return;
    }
  }

  gst_pip_player_reset (self);

  self->primary_info = primary_info;
  self->secondary_info = secondary_info;
  self->primary_uri = g_strdup (data->primary_uri);
  self->secondary_uri = g_strdup (data->secondary_uri);

  if (!gst_pip_player_create_pipeline (self)) {
    gst_pip_player_reset (self);
  } else {
    data->ret = TRUE;
  }
}

typedef struct
{
  GstPad *pad;
  GstEvent *seek;
} InitialSeekData;

static void
do_initial_seek_async (GstElement * elem, InitialSeekData * data)
{
  if (!gst_pad_send_event (data->pad, data->seek))
    GST_ERROR_OBJECT (data->pad, "Initial seek failed");

  gst_object_unref (data->pad);
  g_free (data);
}

static GstPadProbeReturn
on_pad_blocked (GstPad * pad, GstPadProbeInfo * info, GstEvent * seek)
{
  GstElement *parent = gst_pad_get_parent_element (pad);
  InitialSeekData *data = g_new0 (InitialSeekData, 1);

  data->pad = (GstPad *) gst_object_ref (pad);
  data->seek = seek;

  gst_element_call_async (parent,
      (GstElementCallAsyncFunc) do_initial_seek_async, data, nullptr);
  gst_object_unref (parent);

  return GST_PAD_PROBE_REMOVE;
}

static void
on_primary_decodebin_pad_added (GstElement * dbin,
    GstPad * pad, GstPipPlayer * self)
{
  GstEvent *seek;

  if (self->saw_primary)
    return;

  if (!g_str_has_prefix (GST_PAD_NAME (pad), "video"))
    return;

  gst_pad_link (pad, self->primary_pad);
  self->saw_primary = TRUE;

  seek = gst_event_new_seek (self->rate,
      GST_FORMAT_TIME,
      (GstSeekFlags) (GST_SEEK_FLAG_ACCURATE | GST_SEEK_FLAG_FLUSH),
      GST_SEEK_TYPE_SET,
      self->segment_start, GST_SEEK_TYPE_SET, self->segment_stop);

  gst_pad_add_probe (pad, (GstPadProbeType)
      (GST_PAD_PROBE_TYPE_BUFFER | GST_PAD_PROBE_TYPE_BLOCK),
      (GstPadProbeCallback) on_pad_blocked, seek, nullptr);
}

static void
on_secondary_decodebin_pad_added (GstElement * dbin,
    GstPad * pad, GstPipPlayer * self)
{
  GstEvent *seek;

  if (self->saw_secondary)
    return;

  if (!g_str_has_prefix (GST_PAD_NAME (pad), "video"))
    return;

  gst_pad_link (pad, self->secondary_pad);
  self->saw_secondary = TRUE;

  seek = gst_event_new_seek (self->rate,
      GST_FORMAT_TIME,
      (GstSeekFlags) (GST_SEEK_FLAG_ACCURATE | GST_SEEK_FLAG_FLUSH),
      GST_SEEK_TYPE_SET,
      self->secondary_segment_start,
      GST_SEEK_TYPE_SET, self->secondary_segment_stop);

  gst_pad_add_probe (pad, (GstPadProbeType)
      (GST_PAD_PROBE_TYPE_BUFFER | GST_PAD_PROBE_TYPE_BLOCK),
      (GstPadProbeCallback) on_pad_blocked, seek, nullptr);
}

static gboolean
get_stream_resolution (GstDiscovererInfo * info, guint * width, guint * height)
{
  GList *streams;
  GstDiscovererVideoInfo *vinfo;

  streams = gst_discoverer_info_get_video_streams (info);
  if (!streams)
    return FALSE;

  vinfo = GST_DISCOVERER_VIDEO_INFO (streams->data);
  *width = gst_discoverer_video_info_get_width (vinfo);
  *height = gst_discoverer_video_info_get_height (vinfo);

  gst_discoverer_stream_info_list_free (streams);

  return TRUE;
}

static void
initial_seek (GstPipPlayer * self)
{
  GstEvent *seek;

  seek = self->pending_seek;
  self->pending_seek = nullptr;

  if (!seek)
    return;

  if (!self->pipeline || self->current_state < GST_STATE_PAUSED) {
    gst_clear_event (&seek);
    return;
  }

  if (!gst_element_send_event (self->pipeline, seek)) {
    GST_WARNING_OBJECT (self, "Failed to send seek");
  }
}

static gboolean
bus_handler (GstBus * bus, GstMessage * msg, GstPipPlayer * self)
{
  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      GST_INFO_OBJECT (self, "Got EOS");
      self->got_eos = TRUE;
      if (self->app_bus) {
        GstMessage *copy = gst_message_copy (msg);

        gst_bus_post (self->app_bus, copy);
      }
      break;
    case GST_MESSAGE_STATE_CHANGED:{
      GstState old_state, new_state;
      if (GST_MESSAGE_SRC (msg) != GST_OBJECT_CAST (self->pipeline))
        break;

      gst_message_parse_state_changed (msg, &old_state, &new_state, nullptr);
      GST_LOG_OBJECT (self,
          "State %s -> %s",
          gst_element_state_get_name (old_state),
          gst_element_state_get_name (new_state));
      self->current_state = new_state;

      if (new_state == GST_STATE_PAUSED && self->pending_seek)
        initial_seek (self);
      break;
    }
    case GST_MESSAGE_ERROR:{
      GError *err = nullptr;
      gchar *name, *debug = nullptr;

      name = gst_object_get_path_string (msg->src);
      gst_message_parse_error (msg, &err, &debug);
      GST_ERROR_OBJECT (self, "Got error %s: %s (%s)", name, err->message,
          GST_STR_NULL (debug));
      g_clear_error (&err);
      g_free (name);
      g_free (debug);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

static GstPadProbeReturn
seek_event_probe (GstPad * pad, GstPadProbeInfo * info, GstPipPlayer * self)
{
  GstPadProbeReturn ret = GST_PAD_PROBE_OK;
  GstEvent *ev = GST_PAD_PROBE_INFO_EVENT (info);

  switch (GST_EVENT_TYPE (ev)) {
    case GST_EVENT_SEEK:{
      GstPad *peer = gst_pad_get_peer (pad);

      if (peer) {
        guint32 seqnum;
        gdouble rate;
        GstFormat format;
        GstSeekFlags flags;
        GstSeekType start_type, stop_type;
        gint64 start, stop;

        seqnum = gst_event_get_seqnum (ev);
        gst_event_parse_seek (ev, &rate, &format, &flags, &start_type, &start,
            &stop_type, &stop);
        if (format != GST_FORMAT_TIME) {
          GST_DEBUG_OBJECT (self, "Drop non-time format seek");
        } else {
          GstEvent *seek;
          if (rate >= 0) {
            seek = gst_event_new_seek (rate,
                format,
                flags,
                start_type,
                start + self->segment_start, stop_type, self->segment_stop);
          } else {
            seek = gst_event_new_seek (rate,
                format,
                flags,
                start_type,
                self->segment_start, stop_type, stop + self->segment_start);
          }

          GST_DEBUG_OBJECT (pad,
              "Converted seek event %" GST_PTR_FORMAT
              " -> %" GST_PTR_FORMAT, ev, seek);

          gst_event_set_seqnum (seek, seqnum);
          if (!gst_pad_send_event (peer, seek)) {
            GST_WARNING_OBJECT (peer, "Failed to send seek");
          }
        }

        gst_object_unref (peer);
      }

      ret = GST_PAD_PROBE_DROP;
      break;
    }
    default:
      break;
  }

  return ret;
}

static GstPadProbeReturn
secondary_seek_event_probe (GstPad * pad,
    GstPadProbeInfo * info, GstPipPlayer * self)
{
  GstPadProbeReturn ret = GST_PAD_PROBE_OK;
  GstEvent *ev = GST_PAD_PROBE_INFO_EVENT (info);

  switch (GST_EVENT_TYPE (ev)) {
    case GST_EVENT_SEEK:{
      GstPad *peer = gst_pad_get_peer (pad);

      if (peer) {
        guint32 seqnum;
        gdouble rate;
        GstFormat format;
        GstSeekFlags flags;
        GstSeekType start_type, stop_type;
        gint64 start, stop;

        seqnum = gst_event_get_seqnum (ev);
        gst_event_parse_seek (ev, &rate, &format, &flags, &start_type, &start,
            &stop_type, &stop);
        if (format != GST_FORMAT_TIME) {
          GST_DEBUG_OBJECT (self, "Drop non-time format seek");
        } else {
          GstEvent *seek;
          if (rate >= 0) {
            seek = gst_event_new_seek (rate,
                format,
                flags,
                start_type,
                start + self->secondary_segment_start,
                stop_type, self->secondary_segment_stop);
          } else {
            seek = gst_event_new_seek (rate,
                format,
                flags,
                start_type,
                self->secondary_segment_start,
                stop_type, stop + self->secondary_segment_start);
          }

          GST_DEBUG_OBJECT (pad,
              "Converted seek event %" GST_PTR_FORMAT
              " -> %" GST_PTR_FORMAT, ev, seek);

          gst_event_set_seqnum (seek, seqnum);
          if (!gst_pad_send_event (peer, seek)) {
            GST_WARNING_OBJECT (peer, "Failed to send seek");
          }
        }

        gst_object_unref (peer);
      }

      ret = GST_PAD_PROBE_DROP;
      break;
    }
    default:
      break;
  }

  return ret;
}

static gboolean
gst_pip_player_create_pipeline (GstPipPlayer * self)
{
  GstElement *pipeline;
  GstElement *dbin_primary, *dbin_secondary;
  GstElement *rate_primary, *rate_secondary;
  GstElement *compositor, *filter, *queue, *sink;
  GstPad *sinkpad, *srcpad;
  GstCaps *caps;
  guint primary_width, primary_height;
  guint secondary_width, secondary_height;
  gdouble scale_factor;
  GstClockTime primary_duration = GST_CLOCK_TIME_NONE;
  GstClockTime secondary_duration = GST_CLOCK_TIME_NONE;
  GstClockTime duration;
  gint64 segment_start, segment_stop;
  GstBus *bus;

  if (!get_stream_resolution (self->primary_info, &primary_width,
          &primary_height)) {
    GST_ERROR_OBJECT (self, "Failed to get primary stream resolution");
    return FALSE;
  }

  pipeline = gst_pipeline_new ("pip-player-pipeline");
  bus = gst_element_get_bus (pipeline);
  gst_bus_add_watch (bus, (GstBusFunc) bus_handler, self);

  primary_duration = gst_discoverer_info_get_duration (self->primary_info);

  scale_factor = GET_OPT_SCALE_FACTOR (self);
  secondary_width = (guint) (primary_width * scale_factor);
  secondary_height = (guint) (primary_height * scale_factor);

  dbin_primary = gst_element_factory_make ("uridecodebin3", "dbin-primary");
  g_object_set (dbin_primary, "uri", self->primary_uri, nullptr);
  g_signal_connect (dbin_primary,
      "pad-added", G_CALLBACK (on_primary_decodebin_pad_added), self);

  /* configure videorate in front of compositor otherwise videoaggregator
   * will got confused in case of key-unit trick mode due to non-continuous
   * timestamp */
  rate_primary = gst_element_factory_make ("videorate", "videorate-primary");

  compositor =
      gst_element_factory_make (GET_OPT_VIDEO_COMPOSITOR (self), nullptr);
  g_object_set (compositor, "background", 1 /* black */ , nullptr);

  filter = gst_element_factory_make ("capsfilter", nullptr);
  queue = gst_element_factory_make ("queue", nullptr);

  /* For reverse-playback there should be sufficiently large size queue */
  g_object_set (queue,
      "max-size-time",
      (guint64) 10 * GST_SECOND,
      "max-size-bytes", 0, "max-size-buffers", 0, nullptr);

  sink = gst_element_factory_make (GET_OPT_VIDEO_SINK (self), nullptr);
  caps = gst_caps_from_string (GET_OPT_VIDEO_SINK_CAPS_STR (self));
  if (caps) {
    g_object_set (filter, "caps", caps, nullptr);
    gst_caps_unref (caps);
  }

  if (!GET_OPT_SYNC (self))
    g_object_set (sink, "sync", FALSE, nullptr);

  gst_bin_add_many (GST_BIN (pipeline),
      dbin_primary, rate_primary, compositor, filter, queue, sink, nullptr);
  gst_element_link_many (rate_primary, compositor, filter, queue, sink,
      nullptr);

  self->primary_pad = gst_element_get_static_pad (rate_primary, "sink");
  gst_pad_add_probe (self->primary_pad,
      GST_PAD_PROBE_TYPE_EVENT_UPSTREAM,
      (GstPadProbeCallback) seek_event_probe, self, nullptr);

  srcpad = gst_element_get_static_pad (rate_primary, "src");
  sinkpad = gst_pad_get_peer (srcpad);

  gst_object_unref (sinkpad);
  gst_object_unref (srcpad);

  if (self->secondary_uri) {
    secondary_duration =
        gst_discoverer_info_get_duration (self->secondary_info);

    if (!GST_CLOCK_TIME_IS_VALID (secondary_duration)) {
      GST_ERROR_OBJECT (self, "Unknown secondary stream duration, ignore");
    } else {
      dbin_secondary =
          gst_element_factory_make ("uridecodebin3", "dbin-secondary");
      g_object_set (dbin_secondary, "uri", self->secondary_uri, nullptr);
      g_signal_connect (dbin_secondary,
          "pad-added", G_CALLBACK (on_secondary_decodebin_pad_added), self);
      rate_secondary =
          gst_element_factory_make ("videorate", "videorate-secondary");

      gst_bin_add_many (GST_BIN (pipeline), dbin_secondary, rate_secondary,
          nullptr);
      gst_element_link (rate_secondary, compositor);

      self->secondary_pad = gst_element_get_static_pad (rate_secondary, "sink");
      gst_pad_add_probe (self->secondary_pad,
          GST_PAD_PROBE_TYPE_EVENT_UPSTREAM,
          (GstPadProbeCallback) secondary_seek_event_probe, self, nullptr);

      srcpad = gst_element_get_static_pad (rate_secondary, "src");
      sinkpad = gst_pad_get_peer (srcpad);

      g_object_set (sinkpad,
          "width",
          secondary_width,
          "height", secondary_height, "repeat-after-eos", TRUE, nullptr);

      gst_object_unref (sinkpad);
      gst_object_unref (srcpad);
    }
  }

  duration = primary_duration;

  segment_start = GET_OPT_SEGMENT_START (self);
  segment_stop = GET_OPT_SEGMENT_STOP (self);

  /* Build timeline based on user segment-{start,stop} option */
  if (segment_start < (gint64) duration && segment_start >= 0) {
    self->segment_start = (GstClockTime) segment_start;
  } else {
    self->segment_start = 0;
  }

  if (segment_stop > 0 && segment_stop > (gint64) self->segment_start &&
      segment_stop <= (gint64) duration) {
    self->segment_stop = (GstClockTime) segment_stop;
  } else {
    self->segment_stop = duration;
  }

  self->duration = self->segment_stop - self->segment_start;

  GST_INFO_OBJECT (self,
      "Timeline %" GST_TIME_FORMAT " - %" GST_TIME_FORMAT
      ", duration %" GST_TIME_FORMAT
      ", file duration %" GST_TIME_FORMAT,
      GST_TIME_ARGS (self->segment_start),
      GST_TIME_ARGS (self->segment_stop),
      GST_TIME_ARGS (self->duration), GST_TIME_ARGS (duration));

  if (GST_CLOCK_TIME_IS_VALID (secondary_duration)) {
    gint64 secondary_segment_start, secondary_segment_stop, timestamp_offset;

    timestamp_offset = GET_OPT_TIMESTMAP_OFFSET (self);
    if (timestamp_offset == 0) {
      self->secondary_segment_start =
          MIN (self->segment_start, secondary_duration);
      self->secondary_segment_stop =
          MIN (self->segment_stop, secondary_duration);
    } else {
      secondary_segment_start = (gint64) self->segment_start;

      secondary_segment_start += timestamp_offset;
      secondary_segment_start =
          MIN (secondary_segment_start, (gint64) secondary_duration);
      secondary_segment_stop =
          MIN (secondary_segment_start + (gint64) self->duration,
          (gint64) secondary_duration);

      self->secondary_segment_start = (GstClockTime) secondary_segment_start;
      self->secondary_segment_stop = (GstClockTime) secondary_segment_stop;
    }

    GST_INFO_OBJECT (self,
        "Secondary timeline %" GST_TIME_FORMAT
        " - %" GST_TIME_FORMAT
        ", timestamp offset %" GST_STIME_FORMAT
        ", file duration %" GST_TIME_FORMAT,
        GST_TIME_ARGS (self->secondary_segment_start),
        GST_TIME_ARGS (self->secondary_segment_stop),
        GST_STIME_ARGS (timestamp_offset), GST_TIME_ARGS (secondary_duration));
  }

  self->pipeline = pipeline;

  return TRUE;
}

GstPipPlayer *
gst_pip_player_new (GstStructure * config, GstBus * application_bus)
{
  GstPipPlayer *self;
  const gchar *decoder_factory;
  GstPluginFeature *feature;

  self = (GstPipPlayer *) g_object_new (GST_TYPE_PIP_PLAYER, nullptr);
  gst_object_ref_sink (self);

  if (config)
    gst_pip_player_set_config (self, config);

  decoder_factory = GET_OPT_VIDEO_DECODER (self);
  feature = gst_registry_find_feature (gst_registry_get (), decoder_factory,
      GST_TYPE_ELEMENT_FACTORY);
  if (feature) {
    gst_plugin_feature_set_rank (feature, G_MAXINT);
    gst_object_unref (feature);
  }

  /* We will forward some messages to application bus, then application
   * can dispatch it from its own thread */
  if (application_bus)
    self->app_bus = (GstBus *) gst_object_ref (application_bus);

  return self;
}

static gboolean
copy_config (GQuark field_id, const GValue * value, GstPipPlayer * self)
{
  gst_structure_id_set_value (self->config, field_id, value);

  return TRUE;
}

gboolean
gst_pip_player_set_config (GstPipPlayer * player, GstStructure * config)
{
  g_return_val_if_fail (GST_IS_PIP_PLAYER (player), FALSE);
  g_return_val_if_fail (config != nullptr, FALSE);

  gst_structure_foreach (config, (GstStructureForeachFunc) copy_config, player);
  gst_structure_free (config);

  return TRUE;
}

GstStructure *
gst_pip_player_get_config (GstPipPlayer * player)
{
  g_return_val_if_fail (GST_IS_PIP_PLAYER (player), nullptr);

  if (!player->config)
    return nullptr;

  return gst_structure_copy (player->config);
}

gboolean
gst_pip_player_set_uris (GstPipPlayer * player,
    const gchar * primary_uri, const gchar * secondary_uri, GError ** err)
{
  SetUrisData data;

  g_return_val_if_fail (GST_IS_PIP_PLAYER (player), FALSE);
  g_return_val_if_fail (primary_uri != nullptr, FALSE);

  g_rec_mutex_lock (&player->api_lock);

  if (g_strcmp0 (primary_uri, player->primary_uri) == 0 &&
      g_strcmp0 (secondary_uri, player->secondary_uri) == 0) {
    g_rec_mutex_unlock (&player->api_lock);
    return TRUE;
  }

  data.self = player;
  data.primary_uri = primary_uri;
  data.secondary_uri = secondary_uri;
  data.err = err;
  data.ret = FALSE;

  run_sync (player, (ThreadFunc) setup_pipeline, &data);

  if (!data.ret) {
    g_rec_mutex_unlock (&player->api_lock);
    return FALSE;
  }

  /* TODO: do state change */
  g_rec_mutex_unlock (&player->api_lock);
  return TRUE;
}

static gboolean
do_play (GstPipPlayer * self)
{
  if (!self->pipeline) {
    GST_INFO_OBJECT (self, "No pipeline configured");
    return G_SOURCE_REMOVE;
  }

  if (self->got_eos) {
    GstEvent *seek = gst_event_new_seek (self->rate,
        GST_FORMAT_TIME,
        (GstSeekFlags) (GST_SEEK_FLAG_ACCURATE | GST_SEEK_FLAG_FLUSH),
        GST_SEEK_TYPE_SET,
        0,
        GST_SEEK_TYPE_SET,
        self->duration);
    gst_element_send_event (self->pipeline, seek);
    self->got_eos = FALSE;
  }

  self->target_state = GST_STATE_PLAYING;
  gst_element_set_state (self->pipeline, GST_STATE_PLAYING);

  return G_SOURCE_REMOVE;
}

void
gst_pip_player_play (GstPipPlayer * player)
{
  g_return_if_fail (GST_IS_PIP_PLAYER (player));

  g_main_context_invoke (player->context, (GSourceFunc) do_play, player);
}

static gboolean
do_pause (GstPipPlayer * self)
{
  if (!self->pipeline)
    return G_SOURCE_REMOVE;

  self->target_state = GST_STATE_PAUSED;
  gst_element_set_state (self->pipeline, GST_STATE_PAUSED);

  return G_SOURCE_REMOVE;
}

void
gst_pip_player_pause (GstPipPlayer * player)
{
  g_return_if_fail (GST_IS_PIP_PLAYER (player));

  g_main_context_invoke (player->context, (GSourceFunc) do_pause, player);
}

GstClockTime
gst_pip_player_get_duration (GstPipPlayer * player)
{
  g_return_val_if_fail (GST_IS_PIP_PLAYER (player), GST_CLOCK_TIME_NONE);

  return player->duration;
}

typedef struct
{
  GstPipPlayer *self;
  GstClockTime pos;
} GetPositionData;

static void
get_position (GetPositionData * data)
{
  GstPipPlayer *self = data->self;
  gint64 pos;

  if (!self->pipeline || self->current_state < GST_STATE_PAUSED) {
    data->pos = self->last_position;
  } else if (gst_element_query_position (self->pipeline, GST_FORMAT_TIME, &pos)) {
    data->pos = self->last_position = pos;
  } else {
    data->pos = self->last_position;
  }
}

GstClockTime
gst_pip_player_get_position (GstPipPlayer * player)
{
  GetPositionData data;

  g_return_val_if_fail (GST_IS_PIP_PLAYER (player), GST_CLOCK_TIME_NONE);

  data.self = player;
  data.pos = GST_CLOCK_TIME_NONE;

  g_rec_mutex_lock (&player->api_lock);
  run_sync (player, (ThreadFunc) get_position, &data);
  g_rec_mutex_unlock (&player->api_lock);

  return data.pos;
}

typedef struct
{
  GstPipPlayer *self;
  gdouble rate;
  GstSeekFlags flags;
} SetRateData;

static void
set_rate (SetRateData * data)
{
  GstPipPlayer *self = data->self;
  gint64 pos;
  GstEvent *seek;
  guint seek_flags = GST_SEEK_FLAG_FLUSH;

  self->rate = data->rate;
  self->seek_flags = (guint) data->flags;

  if (!self->pipeline || self->current_state < GST_STATE_PAUSED)
    return;

  gst_element_set_state (self->pipeline, GST_STATE_PAUSED);
  if (!gst_element_query_position (self->pipeline, GST_FORMAT_TIME, &pos) ||
      pos < 0) {
    GST_WARNING_OBJECT (self, "Failed to get current position");
    return;
  }

  gst_clear_event (&self->pending_seek);
  if ((self->seek_flags & GST_SEEK_FLAG_TRICKMODE_KEY_UNITS) != 0)
    seek_flags |= GST_SEEK_FLAG_TRICKMODE_KEY_UNITS;

  if ((self->seek_flags & GST_SEEK_FLAG_ACCURATE) != 0)
    seek_flags |= GST_SEEK_FLAG_ACCURATE;

  if (self->rate != 1.0)
    seek_flags |= GST_SEEK_FLAG_TRICKMODE;

  GST_DEBUG_OBJECT (self, "Setting rate %lf, flags 0x%x", self->rate,
      seek_flags);

  if (self->rate >= 0) {
    seek = gst_event_new_seek (self->rate,
        GST_FORMAT_TIME,
        (GstSeekFlags) seek_flags,
        GST_SEEK_TYPE_SET, pos, GST_SEEK_TYPE_SET, self->duration);
  } else {
    seek = gst_event_new_seek (self->rate,
        GST_FORMAT_TIME,
        (GstSeekFlags) seek_flags,
        GST_SEEK_TYPE_SET, 0, GST_SEEK_TYPE_SET, pos);
  }

  if (!gst_element_send_event (self->pipeline, seek))
    GST_WARNING_OBJECT (self, "Failed to send seek event");

  if (self->target_state >= GST_STATE_PLAYING)
    gst_element_set_state (self->pipeline, GST_STATE_PLAYING);
}

void
gst_pip_player_set_rate (GstPipPlayer * player, gdouble rate,
    GstSeekFlags flags)
{
  SetRateData data;

  g_return_if_fail (GST_IS_PIP_PLAYER (player));

  g_rec_mutex_lock (&player->api_lock);
  data.self = player;
  data.rate = rate;
  data.flags = flags;

  run_sync (player, (ThreadFunc) set_rate, &data);
  g_rec_mutex_unlock (&player->api_lock);
}

typedef struct
{
  GstPipPlayer *self;
  GstClockTime pos;
} SeekData;

static void
do_seek (SeekData * data)
{
  GstPipPlayer *self = data->self;
  GstEvent *seek;
  GstClockTime pos;
  guint seek_flags = GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_ACCURATE;

  if (!self->pipeline || self->current_state < GST_STATE_PAUSED)
    return;

  pos = MIN (data->pos, self->duration);

  gst_element_set_state (self->pipeline, GST_STATE_PAUSED);
  gst_clear_event (&self->pending_seek);
  if ((self->seek_flags & (guint) GST_SEEK_FLAG_TRICKMODE_KEY_UNITS) != 0)
    seek_flags |= GST_SEEK_FLAG_TRICKMODE_KEY_UNITS;

  if (self->rate != 1.0)
    seek_flags |= GST_SEEK_FLAG_TRICKMODE;

  if (self->rate >= 0) {
    seek = gst_event_new_seek (self->rate,
        GST_FORMAT_TIME,
        (GstSeekFlags) seek_flags,
        GST_SEEK_TYPE_SET, pos, GST_SEEK_TYPE_SET, self->duration);
  } else {
    seek = gst_event_new_seek (self->rate,
        GST_FORMAT_TIME,
        (GstSeekFlags) seek_flags,
        GST_SEEK_TYPE_SET, 0, GST_SEEK_TYPE_SET, pos);
  }

  GST_INFO_OBJECT (self, "Seek to %" GST_TIME_FORMAT, GST_TIME_ARGS (pos));

  if (!gst_element_send_event (self->pipeline, seek))
    GST_WARNING_OBJECT (self, "Failed to send seek event");

  if (self->target_state >= GST_STATE_PLAYING)
    gst_element_set_state (self->pipeline, GST_STATE_PLAYING);
}

void
gst_pip_player_seek (GstPipPlayer * player, GstClockTime position)
{
  SeekData data;

  g_return_if_fail (GST_IS_PIP_PLAYER (player));

  g_rec_mutex_lock (&player->api_lock);
  data.self = player;
  data.pos = position;

  run_sync (player, (ThreadFunc) do_seek, &data);
  g_rec_mutex_unlock (&player->api_lock);
}

typedef struct
{
  GstPipPlayer *self;
  GstFormat format;
  guint64 amount;
} StepData;

static void
do_step (StepData * data)
{
  GstPipPlayer *self = data->self;
  GstEvent *step;

  if (!self->pipeline || self->current_state < GST_STATE_PAUSED)
    return;

  gst_element_set_state (self->pipeline, GST_STATE_PAUSED);
  gst_clear_event (&self->pending_seek);

  step = gst_event_new_step (data->format, data->amount, 1.0, TRUE, FALSE);

  if (!gst_element_send_event (self->pipeline, step))
    GST_WARNING_OBJECT (self, "Failed to send step event");
}

void
gst_pip_player_step (GstPipPlayer * player, GstFormat format, guint64 amount)
{
  StepData data;

  g_return_if_fail (GST_IS_PIP_PLAYER (player));

  g_rec_mutex_lock (&player->api_lock);
  data.self = player;
  data.format = format;
  data.amount = amount;

  run_sync (player, (ThreadFunc) do_step, &data);
  g_rec_mutex_unlock (&player->api_lock);
}
