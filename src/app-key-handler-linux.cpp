#include "app-key-handler.h"
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

typedef struct _LinuxKeyHandler
{
  gulong watch_id;
  struct termios term_settings;
  KeyInputCallback callback;
  GMainContext *context;
  GSource *source;
  gpointer user_data;
} LinuxKeyHandler;

static LinuxKeyHandler *_handler = nullptr;

static gboolean
_handlerio_func (GIOChannel * channel,
    GIOCondition condition, LinuxKeyHandler * handler)
{
  if (condition & G_IO_IN) {
    GIOStatus status;
    gchar buf[16] = {
      0,
    };
    gsize read;

    status =
        g_io_channel_read_chars (channel, buf, sizeof (buf) - 1, &read,
        nullptr);
    if (status == G_IO_STATUS_ERROR) {
      return G_SOURCE_REMOVE;
    }

    if (status == G_IO_STATUS_NORMAL) {
      gchar value;
      gboolean is_ascii = FALSE;

      if (g_strcmp0 (buf, "\033[A") == 0) {
        value = (gchar) KB_ARROW_UP;
      } else if (g_strcmp0 (buf, "\033[B") == 0) {
        value = (gchar) KB_ARROW_DOWN;
      } else if (g_strcmp0 (buf, "\033[D") == 0) {
        value = (gchar) KB_ARROW_LEFT;
      } else if (g_strcmp0 (buf, "\033[C") == 0) {
        value = (gchar) KB_ARROW_RIGHT;
      } else {
        value = buf[0];
        is_ascii = TRUE;
      }

      handler->callback (value, is_ascii, handler->user_data);
    }
  }

  return G_SOURCE_CONTINUE;
}

void
set_key_handler (GMainContext * context,
    KeyInputCallback callback, gpointer user_data)
{
  struct termios new_settings;
  struct termios old_settings;
  GIOChannel *io_channel;

  if (_handler || !callback)
    return;

  if (tcgetattr (STDIN_FILENO, &old_settings) != 0)
    return;

  new_settings = old_settings;
  new_settings.c_lflag &= ~(ECHO | ICANON | IEXTEN);
  new_settings.c_cc[VMIN] = 0;
  new_settings.c_cc[VTIME] = 0;

  if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &new_settings) != 0)
    return;

  setvbuf (stdin, nullptr, _IONBF, 0);

  _handler = g_new0 (LinuxKeyHandler, 1);
  if (!context)
    context = g_main_context_default ();

  _handler->context = g_main_context_ref (context);
  _handler->term_settings = old_settings;
  _handler->callback = callback;
  _handler->user_data = user_data;

  io_channel = g_io_channel_unix_new (STDIN_FILENO);

  _handler->source = g_io_create_watch (io_channel, G_IO_IN);
  g_io_channel_unref (io_channel);

  g_source_set_callback (_handler->source, (GSourceFunc) _handlerio_func,
      _handler, nullptr);
  g_source_attach (_handler->source, context);
}

void
unset_key_handler (void)
{
  if (!_handler)
    return;

  if (_handler->source) {
    g_source_destroy (_handler->source);
    g_source_unref (_handler->source);
  }

  tcsetattr (STDIN_FILENO, TCSAFLUSH, &_handler->term_settings);
  setvbuf (stdin, nullptr, _IOLBF, 0);

  g_main_context_unref (_handler->context);
  g_clear_pointer (&_handler, g_free);
}
