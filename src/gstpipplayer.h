#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

#define GST_TYPE_PIP_PLAYER             (gst_pip_player_get_type ())
#define GST_IS_PIP_PLAYER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_PIP_PLAYER))
#define GST_IS_PIP_PLAYER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_TYPE_PIP_PLAYER))
#define GST_PIP_PLAYER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_TYPE_PIP_PLAYER, GstPipPlayerClass))
#define GST_PIP_PLAYER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_PIP_PLAYER, GstPipPlayer))
#define GST_PIP_PLAYER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GST_TYPE_PIP_PLAYER, GstPipPlayerClass))
#define GST_PIP_PLAYER_CAST(obj)        ((GstPipPlayer*)(obj))

typedef struct _GstPipPlayer GstPipPlayer;
typedef struct _GstPipPlayerClass GstPipPlayerClass;

#define GST_PIP_PLAYER_VIDEO_DECODER "GstPipPlayer.video-decoder"
#define GST_PIP_PLAYER_VIDEO_COMPOSITOR "GstPipPlayer.video-compositor"
#define GST_PIP_PLAYER_VIDEO_SINK "GstPipPlayer.video-sink"
#define GST_PIP_PLAYER_VIDEO_SINK_CAPS_STR "GstPipPlayer.video-sink-caps"
#define GST_PIP_PLAYER_OVERLAY_SCALE_FACTOR "GstPipPlayer.overlay-scale-factor"
#define GST_PIP_PLAYER_SYNC "GstPipPlayer.sync"
#define GST_PIP_PLAYER_TIMESTAMP_OFFSET "GstPipPlayer.timestamp-offset"
#define GST_PIP_PLAYER_SEGMENT_START "GstPipPlayer.segment-start"
#define GST_PIP_PLAYER_SEGMENT_STOP "GstPipPlayer.segment-stop"

GstPipPlayer *      gst_pip_player_new (GstStructure * config,
                                        GstBus * application_bus);

gboolean            gst_pip_player_set_config (GstPipPlayer * player,
                                               GstStructure * config);

GstStructure *      gst_pip_player_get_config (GstPipPlayer * player);

gboolean            gst_pip_player_set_uris (GstPipPlayer * player,
                                             const gchar * primary_uri,
                                             const gchar * secondary_uri,
                                             GError ** err);

void                gst_pip_player_play (GstPipPlayer * player);

void                gst_pip_player_pause (GstPipPlayer * player);

GstClockTime        gst_pip_player_get_duration (GstPipPlayer * player);

GstClockTime        gst_pip_player_get_position (GstPipPlayer * player);

void                gst_pip_player_set_rate (GstPipPlayer * player,
                                             gdouble rate,
                                             GstSeekFlags flags);

void                gst_pip_player_seek     (GstPipPlayer * player,
                                             GstClockTime position);

void                gst_pip_player_step     (GstPipPlayer * player,
                                             GstFormat format,
                                             guint64 amount);

G_END_DECLS

