#include "app-key-handler.h"
#include "gstpipplayer.h"
#include <algorithm>
#include <gst/gst.h>
#include <string>
#include <utility>
#include <vector>

typedef enum
{
  TRICK_MODE_NORMAL = 0,
  TRICK_MODE_REVERSE,
  TRICK_MODE_KEY_UNIT,
  TRICK_MODE_KEY_UNIT_REVERS,
  TRICK_MODE_STEP,
  TRICK_MODE_STEP_REVERSE,
  TRICK_MODE_LAST,
} TrickMode;

static const gchar *
trick_mode_get_name (TrickMode mode)
{
  switch (mode) {
    case TRICK_MODE_NORMAL:
      return "normal";
    case TRICK_MODE_REVERSE:
      return "reverse";
    case TRICK_MODE_KEY_UNIT:
      return "key-unit";
    case TRICK_MODE_KEY_UNIT_REVERS:
      return "key-unit-reverse";
    case TRICK_MODE_STEP:
      return "step";
    case TRICK_MODE_STEP_REVERSE:
      return "step-reverse";
    default:
      break;
  }

  return "none";
}

typedef struct
{
  GstPipPlayer *player;
  GMainLoop *loop;
  GMainContext *context;
  GstBus *bus;

  TrickMode trick_mode;
  guint rate;
  gboolean benchmark;

  GstState target_state;
} AppData;

static gint
feature_name_compare_func (gconstpointer p1, gconstpointer p2)
{
  return g_strcmp0 (gst_plugin_feature_get_name (p1),
      gst_plugin_feature_get_name (p2));
}

static guint
check_d3d11_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  GList *features;
  std::vector < std::pair < std::string, std::string >> list;
  GList *iter;
  guint num_dec;

  features =
      gst_registry_get_feature_list_by_plugin (gst_registry_get (), "d3d11");
  if (!features)
    return 0;

  features = g_list_sort (features, (GCompareFunc) feature_name_compare_func);
  for (iter = features; iter; iter = g_list_next (iter)) {
    GstPluginFeature *f = GST_PLUGIN_FEATURE (iter->data);
    const gchar *name;
    const gchar *long_name;
    GstElementFactory *factory;

    if (!GST_IS_ELEMENT_FACTORY (f))
      continue;

    name = gst_plugin_feature_get_name (f);
    factory = GST_ELEMENT_FACTORY (f);

    if (!g_str_has_prefix (name, "d3d11h264"))
      continue;

    long_name =
        gst_element_factory_get_metadata (factory,
        GST_ELEMENT_METADATA_LONGNAME);
    list.emplace_back (std::make_pair (name, long_name));
  }

  gst_plugin_feature_list_free (features);

  if (list.empty ())
    return 0;

for (const auto & iter:list)
    decoders.push_back (iter.first);

  num_dec = (guint) list.size ();
  if (do_print) {
    gst_println ("[Found \"%d\" D3D11 H.264 Decoder(s)]", num_dec);
  for (const auto & iter:list)
      gst_println ("%s: %s", iter.first.c_str (), iter.second.c_str ());
    gst_print ("\n");
  }

  return num_dec;
}

static guint
check_nvidia_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  GList *features;
  std::vector < std::pair < std::string, std::string >> list;
  GList *iter;
  guint num_dec;

  features =
      gst_registry_get_feature_list_by_plugin (gst_registry_get (), "nvcodec");
  if (!features)
    return 0;

  features = g_list_sort (features, (GCompareFunc) feature_name_compare_func);
  for (iter = features; iter; iter = g_list_next (iter)) {
    GstPluginFeature *f = GST_PLUGIN_FEATURE (iter->data);
    const gchar *name;
    const gchar *long_name;
    GstElementFactory *factory;

    if (!GST_IS_ELEMENT_FACTORY (f))
      continue;

    name = gst_plugin_feature_get_name (f);
    factory = GST_ELEMENT_FACTORY (f);

    if (!g_str_has_prefix (name, "nvh264") || !g_str_has_suffix (name, "dec"))
      continue;

    long_name =
        gst_element_factory_get_metadata (factory,
        GST_ELEMENT_METADATA_LONGNAME);
    list.emplace_back (std::make_pair (name, long_name));
  }

  gst_plugin_feature_list_free (features);

  if (list.empty ())
    return 0;

for (const auto & iter:list)
    decoders.push_back (iter.first);

  num_dec = (guint) list.size ();
  if (do_print) {
    gst_println ("[Found \"%d\" NVDEC H.264 Decoder(s)]", num_dec);
  for (const auto & iter:list)
      gst_println ("%s: %s", iter.first.c_str (), iter.second.c_str ());
    gst_print ("\n");
  }

  return num_dec;
}

static guint
check_va_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  GList *features;
  std::vector < std::pair < std::string, std::string >> list;
  GList *iter;
  guint num_dec;

  features =
      gst_registry_get_feature_list_by_plugin (gst_registry_get (), "va");
  if (!features)
    return 0;

  features = g_list_sort (features, (GCompareFunc) feature_name_compare_func);
  for (iter = features; iter; iter = g_list_next (iter)) {
    GstPluginFeature *f = GST_PLUGIN_FEATURE (iter->data);
    const gchar *name;
    const gchar *long_name;
    GstElementFactory *factory;

    if (!GST_IS_ELEMENT_FACTORY (f))
      continue;

    name = gst_plugin_feature_get_name (f);
    factory = GST_ELEMENT_FACTORY (f);

    if (!g_str_has_prefix (name, "va") || !g_str_has_suffix (name, "h264dec"))
      continue;

    long_name =
        gst_element_factory_get_metadata (factory,
        GST_ELEMENT_METADATA_LONGNAME);
    list.emplace_back (std::make_pair (name, long_name));
  }

  gst_plugin_feature_list_free (features);

  if (list.empty ())
    return 0;

for (const auto & iter:list)
    decoders.push_back (iter.first);

  num_dec = (guint) list.size ();
  if (do_print) {
    gst_println ("[Found \"%d\" VA H.264 Decoder(s)]", num_dec);
  for (const auto & iter:list)
      gst_println ("%s: %s", iter.first.c_str (), iter.second.c_str ());
    gst_print ("\n");
  }

  return num_dec;
}

static guint
check_libav_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  GstPluginFeature *feature;

  feature =
      gst_registry_find_feature (gst_registry_get (), "avdec_h264",
      GST_TYPE_ELEMENT_FACTORY);

  if (!feature)
    return 0;

  decoders.push_back ("avdec_h264");

  if (do_print) {
    GstElementFactory *factory;
    const gchar *long_name;

    factory = GST_ELEMENT_FACTORY (feature);
    long_name =
        gst_element_factory_get_metadata (factory,
        GST_ELEMENT_METADATA_LONGNAME);

    gst_println ("[Found LIBAV H.264 Decoder]");
    gst_println ("%s: %s\n", "avdec_h264", long_name);
  }

  gst_object_unref (feature);

  return 1;
}

static guint
check_vaapi_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  std::vector < std::pair < std::string, std::string >> list;
  /* preference order */
  const gchar *to_check[] = { "vaapidecodebin", "vaapih264dec" };
  guint num_elem;

  for (guint i = 0; i < G_N_ELEMENTS (to_check); i++) {
    GstPluginFeature *feature;
    feature =
        gst_registry_find_feature (gst_registry_get (), to_check[i],
        GST_TYPE_ELEMENT_FACTORY);
    if (feature) {
      GstElementFactory *factory = GST_ELEMENT_FACTORY (feature);
      const gchar *long_name = gst_element_factory_get_metadata (factory,
          GST_ELEMENT_METADATA_LONGNAME);

      list.emplace_back (std::make_pair (to_check[i], long_name));
      gst_object_unref (feature);
    }
  }

  if (list.empty ())
    return 0;

for (const auto & iter:list)
    decoders.push_back (iter.first);

  num_elem = (guint) list.size ();

  if (do_print) {
    gst_println ("[Found \"%d\" VAAPI H.264 decoders(s)]", num_elem);
  for (const auto & iter:list)
      gst_println ("%s: %s", iter.first.c_str (), iter.second.c_str ());
    gst_print ("\n");
  }

  return num_elem;
}

static guint
check_msdk_decoder (gboolean do_print, std::vector < std::string > &decoders)
{
  GstPluginFeature *feature;

  feature =
      gst_registry_find_feature (gst_registry_get (), "msdkh264dec",
      GST_TYPE_ELEMENT_FACTORY);

  if (!feature)
    return 0;

  decoders.push_back ("msdkh264dec");

  if (do_print) {
    GstElementFactory *factory;
    const gchar *long_name;

    factory = GST_ELEMENT_FACTORY (feature);
    long_name =
        gst_element_factory_get_metadata (factory,
        GST_ELEMENT_METADATA_LONGNAME);

    gst_println ("[Found MSDK H.264 Decoder]");
    gst_println ("%s: %s\n", "msdkh264dec", long_name);
  }

  gst_object_unref (feature);

  return 1;
}

static guint
check_compositor (gboolean do_print, std::vector < std::string > &compositors)
{
  std::vector < std::pair < std::string, std::string >> list;
  /* preference order */
  const gchar *to_check[] = { "d3d11compositor", "glvideomixer", "compositor" };
  guint num_compositor;

  for (guint i = 0; i < G_N_ELEMENTS (to_check); i++) {
    GstPluginFeature *feature;
    feature =
        gst_registry_find_feature (gst_registry_get (), to_check[i],
        GST_TYPE_ELEMENT_FACTORY);
    if (feature) {
      GstElementFactory *factory = GST_ELEMENT_FACTORY (feature);
      const gchar *long_name = gst_element_factory_get_metadata (factory,
          GST_ELEMENT_METADATA_LONGNAME);

      list.emplace_back (std::make_pair (to_check[i], long_name));
      gst_object_unref (feature);
    }
  }

  if (list.empty ())
    return 0;

for (const auto & iter:list)
    compositors.push_back (iter.first);

  num_compositor = (guint) list.size ();
  if (do_print) {
    gst_println ("[Found \"%d\" Compositor element(s)]", num_compositor);
  for (const auto & iter:list)
      gst_println ("%s: %s", iter.first.c_str (), iter.second.c_str ());
    gst_print ("\n");
  }

  return num_compositor;
}

static void
do_print_plugin (void)
{
  std::vector < std::string > dummy;

  gst_println ("Checking available hardware decoders...\n");
  check_d3d11_decoder (TRUE, dummy);
  check_nvidia_decoder (TRUE, dummy);
  check_va_decoder (TRUE, dummy);
  check_vaapi_decoder (TRUE, dummy);
  check_libav_decoder (TRUE, dummy);
  check_msdk_decoder (TRUE, dummy);

  gst_println ("Checking available composing elements...\n");
  check_compositor (TRUE, dummy);
}

static void
get_available_decoders (std::vector < std::string > &decoders)
{
  check_d3d11_decoder (FALSE, decoders);
  check_nvidia_decoder (FALSE, decoders);
  check_va_decoder (FALSE, decoders);
  check_vaapi_decoder (FALSE, decoders);
  check_msdk_decoder (FALSE, decoders);
  check_libav_decoder (FALSE, decoders);
}

static void
get_available_compositors (std::vector < std::string > &compositors)
{
  check_compositor (FALSE, compositors);
}

static void
perform_seek (AppData * data, gdouble position_ratio)
{
  GstPipPlayer *player = data->player;
  gint rate = data->rate;
  GstClockTime pos, dur;
  GstClockTime target;

  switch (data->trick_mode) {
    case TRICK_MODE_REVERSE:
    case TRICK_MODE_KEY_UNIT_REVERS:
    case TRICK_MODE_STEP_REVERSE:
      rate *= -1;
      break;
    default:
      break;
  }

  gst_pip_player_pause (player);

  pos = gst_pip_player_get_position (player);
  if (!GST_CLOCK_TIME_IS_VALID (pos)) {
    gst_println ("Failed to get current position");
    return;
  }

  dur = gst_pip_player_get_duration (player);
  if (!GST_CLOCK_TIME_IS_VALID (dur)) {
    gst_println ("Failed to query duration");
    return;
  }

  target = pos;
  if (position_ratio != 0) {
    gint64 step = (gint64) (dur * position_ratio);

    if (ABS (step) < GST_SECOND)
      step = (position_ratio > 0) ? GST_SECOND : -GST_SECOND;

    target += step;
  }

  if (target < 0)
    target = 0;
  if (target > dur)
    target = dur;

  gst_pip_player_seek (player, target);
  gst_pip_player_play (player);
}

static void
update_rate (AppData * data)
{
  guint seek_flags = GST_SEEK_FLAG_ACCURATE;
  gdouble rate;

  rate = data->rate;

  switch (data->trick_mode) {
    case TRICK_MODE_KEY_UNIT:
    case TRICK_MODE_KEY_UNIT_REVERS:
      seek_flags |= GST_SEEK_FLAG_TRICKMODE_KEY_UNITS;
      break;
    default:
      break;
  }

  switch (data->trick_mode) {
    case TRICK_MODE_REVERSE:
    case TRICK_MODE_KEY_UNIT_REVERS:
    case TRICK_MODE_STEP_REVERSE:
      rate *= -1;
      break;
    default:
      break;
  }

  gst_pip_player_set_rate (data->player, rate, (GstSeekFlags) seek_flags);
}

static void
show_keyboard_help (void)
{
  static struct
  {
    const gchar *key_desc;
    const gchar *key_help;
  } key_controls[] = { {"space", "pause/unpause"},
  {"t or T", "switch trick modes"},
  {"s", "step forward or backward"},
  {"Right arrow", "seek forward"},
  {"Left arrow", "seek backward"},
  {"[1, 9]", "set playback rate"},
  {"q", "quit"}
  };

  guint max_desc_len = 0;

  gst_print ("\n\n%s\n\n", "keyboard controls:");

  for (guint i = 0; i < G_N_ELEMENTS (key_controls); ++i) {
    guint desc_len = g_utf8_strlen (key_controls[i].key_desc, -1);
    max_desc_len = MAX (max_desc_len, desc_len);
  }
  max_desc_len++;

  for (guint i = 0; i < G_N_ELEMENTS (key_controls); ++i) {
    guint padding = max_desc_len - g_utf8_strlen (key_controls[i].key_desc, -1);
    gst_print ("\t%s", key_controls[i].key_desc);
    gst_print ("%-*s: ", padding, "");
    gst_print ("%s\n", key_controls[i].key_help);
  }
  gst_print ("\n");
}

/* Called from our GMainContext thread */
static void
key_input_callback (gchar input, gboolean is_ascii, AppData * data)
{
  if (is_ascii) {
    if (input >= '1' && input <= '9') {

      data->rate = (guint) input - (guint) '0';
      gst_print ("                                     \r");
      gst_println ("Applying rate %d\n", data->rate);
      update_rate (data);
    } else if (input == 'k') {
      show_keyboard_help ();
    } else if (input == 'q') {
      g_main_loop_quit (data->loop);
    } else if (input == 't' || input == 'T') {
      gint trick_mode = (gint) data->trick_mode;
      if (input == 't')
        trick_mode++;
      else
        trick_mode--;

      if (trick_mode < 0)
        trick_mode = (gint) TRICK_MODE_LAST - 1;
      else if (trick_mode >= (gint) TRICK_MODE_LAST)
        trick_mode = (gint) TRICK_MODE_NORMAL;

      data->trick_mode = (TrickMode) trick_mode;

      gst_print ("                                     \r");
      gst_println ("New trick mode: %s",
          trick_mode_get_name (data->trick_mode));

      update_rate (data);
      return;
    } else if (input == 's') {
      if (data->trick_mode != TRICK_MODE_STEP &&
          data->trick_mode != TRICK_MODE_STEP_REVERSE) {
        gst_print ("                                     \r");
        gst_println ("Change trick mode first, current mode: %s",
            trick_mode_get_name (data->trick_mode));
      } else {
        if (data->trick_mode == TRICK_MODE_STEP) {
          gst_print ("                                     \r");
          gst_println ("Step forward");
        } else {
          gst_print ("                                     \r");
          gst_println ("Step backward");
        }
        gst_pip_player_step (data->player, GST_FORMAT_BUFFERS, 1);
      }
    } else if (input == ' ') {
      if (data->target_state == GST_STATE_PLAYING) {
        data->target_state = GST_STATE_PAUSED;
        gst_pip_player_pause (data->player);
      } else {
        data->target_state = GST_STATE_PLAYING;
        gst_pip_player_play (data->player);
      }
    }

    return;
  }

  switch (input) {
    case KB_ARROW_LEFT:
      perform_seek (data, -0.01);
      break;
    case KB_ARROW_RIGHT:
      perform_seek (data, 0.08);
      break;
    default:
      break;
  }
}

static gboolean
query_position_cb (AppData * app_data)
{
  GstClockTime pos, dur;
  GstPipPlayer *player;
  gchar dstr[32], pstr[32];

  player = app_data->player;

  if (!player)
    return G_SOURCE_REMOVE;

  if (app_data->target_state == GST_STATE_PAUSED) {
    gst_print ("Paused\r");
    return G_SOURCE_CONTINUE;
  }

  pos = gst_pip_player_get_position (player);
  dur = gst_pip_player_get_duration (player);

  if (!GST_CLOCK_TIME_IS_VALID (pos) || !GST_CLOCK_TIME_IS_VALID (dur))
    return G_SOURCE_CONTINUE;

  g_snprintf (pstr, 32, "%" GST_TIME_FORMAT, GST_TIME_ARGS (pos));
  pstr[9] = '\0';
  g_snprintf (dstr, 32, "%" GST_TIME_FORMAT, GST_TIME_ARGS (dur));
  dstr[9] = '\0';

  if (dur > 0 && dur >= pos) {
    gdouble percent;
    percent = 100 * (gdouble) (pos) / dur;

    gst_print ("%s / %s (%.1f %%)\r", pstr, dstr, percent);
  } else {
    gst_print ("%s / %s\r", pstr, dstr);
  }

  return G_SOURCE_CONTINUE;
}

static gboolean
bus_handler (GstBus * bus, GstMessage * msg, AppData * data)
{
  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      gst_println ("Got eos");
      if (data->benchmark)
        g_main_loop_quit (data->loop);
      break;
    case GST_MESSAGE_ERROR:{
      GError *err = nullptr;
      gchar *name, *debug = nullptr;

      name = gst_object_get_path_string (msg->src);
      gst_message_parse_error (msg, &err, &debug);
      gst_println ("Got error %s: %s (%s)", name, err->message,
          GST_STR_NULL (debug));
      g_clear_error (&err);
      g_free (name);
      g_free (debug);
      g_main_loop_quit (data->loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

int
main (int argc, char **argv)
{
  gchar **filenames = nullptr;
  gchar *requested_decoder = nullptr;
  gchar *requested_compositor = nullptr;
  gchar *requested_sink = nullptr;
  gchar *videosink_caps = nullptr;
  GError *error = nullptr;
  gboolean show_plugin = FALSE;
  gboolean benchmark = FALSE;
  gdouble scale_factor = 0.4f;
  gint64 timestamp_offset = 0;
  gint64 segment_start = 0;
  gint64 segment_stop = -1;
  std::vector < std::string > uris;
  GOptionContext *ctx;
  GOptionEntry options[] = {
    {"scale-factor",
          0,
          0,
          G_OPTION_ARG_DOUBLE,
          &scale_factor,
          "Scale factor for secondary stream, in [0.0, 1.0] range",
        nullptr},
    {"show-plugins",
          0,
          0,
          G_OPTION_ARG_NONE,
          &show_plugin,
          "Print list of plugins for decoding and composing and exit",
        nullptr},
    {"video-decoder",
          0,
          0,
          G_OPTION_ARG_STRING,
          &requested_decoder,
          "Specify video decoder element to use",
        nullptr},
    {"video-compositor",
          0,
          0,
          G_OPTION_ARG_STRING,
          &requested_compositor,
          "Specify video compositor element to use",
        nullptr},
    {"video-sink",
          0,
          0,
          G_OPTION_ARG_STRING,
          &requested_sink,
          "Specify video sink element to use",
        nullptr},
    {"video-sink-caps",
          0,
          0,
          G_OPTION_ARG_STRING,
          &videosink_caps,
          "Specify caps for video sink element to be negotiated "
          "(example: \"video/x-raw,format=RGBx\")",
        nullptr},
    {"benchmark",
          0,
          0,
          G_OPTION_ARG_NONE,
          &benchmark,
          "Enables benchmark for forward playback",
        nullptr},
    {"timestamp-offset",
          0,
          0,
          G_OPTION_ARG_INT64,
          &timestamp_offset,
        "Specify timestamp offset of overlay relative to primary stream"},
    {"segment-start",
          0,
          0,
          G_OPTION_ARG_INT64,
          &segment_start,
        "Specify segment start time of primary stream"},
    {"segment-stop",
          0,
          0,
          G_OPTION_ARG_INT64,
          &segment_stop,
        "Specify segment end time of primary stream (-1 = unspecified)"},
    {G_OPTION_REMAINING,
          0,
          0,
          G_OPTION_ARG_FILENAME_ARRAY,
          &filenames,
        nullptr},
    {nullptr}
  };
  GstPluginFeature *feature;
  GSource *timeout_source;
  std::vector < std::string > decoders;
  std::vector < std::string > compositors;
  std::string selected_decoder;
  std::string selected_compositor;
  std::string selected_sink;
  GstClockTime before, after, diff;
  guint rank;
  GstPipPlayer *player;
  GstStructure *config;
  GstBus *bus;
  AppData *app_data;
  gchar *config_str;

  ctx = g_option_context_new ("FILE1|URI1 [FILE2|URI2] [FILE3|URI3] ...");
  g_option_context_add_main_entries (ctx, options, nullptr);
  g_option_context_add_group (ctx, gst_init_get_option_group ());
  if (!g_option_context_parse (ctx, &argc, &argv, &error)) {
    gst_print ("Error initializing: %s\n", GST_STR_NULL (error->message));
    g_option_context_free (ctx);
    g_clear_error (&error);
    return 1;
  }
  g_option_context_free (ctx);

  if (show_plugin) {
    do_print_plugin ();
    return 0;
  }

  if (filenames && *filenames) {
    guint len = g_strv_length (filenames);

    /* Will support up to 2 streams */
    for (guint i = 0; i < len && uris.size () < 2; i++) {
      gchar *uri;

      if (gst_uri_is_valid (filenames[i])) {
        uris.push_back (std::string (filenames[i]));
        continue;
      }

      uri = gst_filename_to_uri (filenames[i], nullptr);
      if (uri)
        uris.push_back (std::string (uri));
      g_free (uri);
    }

    g_strfreev (filenames);
  }

  if (uris.size () != 2) {
    gst_println ("Not enough stream");
    return 1;
  }

  if (scale_factor < 0 || scale_factor > 1.0f) {
    gst_println ("Invalid scale factor %lf, fixing", scale_factor);
    scale_factor = 0.4f;
  }

  get_available_decoders (decoders);
  get_available_compositors (compositors);

  if (decoders.empty ()) {
    gst_printerr ("No available h264 decoder element");
    return 1;
  }

  if (compositors.empty ()) {
    gst_printerr ("No available compositor element");
    return 1;
  }

  if (requested_decoder) {
    auto it = std::find (decoders.begin (), decoders.end (), requested_decoder);
    if (it == decoders.end ()) {
      gst_println ("%s is not available, pick %s", requested_decoder,
          decoders[0].c_str ());
      selected_decoder = decoders[0];
    } else {
      selected_decoder = std::string (requested_decoder);
    }
  } else {
    selected_decoder = decoders[0];
  }

  feature =
      gst_registry_find_feature (gst_registry_get (), selected_decoder.c_str (),
      GST_TYPE_ELEMENT_FACTORY);
  rank = gst_plugin_feature_get_rank (feature);
  /* for this decoder to be autoplugged, at least marginal rank is needed */
  if (rank < (guint) GST_RANK_MARGINAL)
    gst_plugin_feature_set_rank (feature, GST_RANK_MARGINAL);
  gst_object_unref (feature);

  if (requested_compositor) {
    auto it = std::find (compositors.begin (), compositors.end (),
        requested_compositor);
    if (it == compositors.end ()) {
      gst_println ("%s is not available, pick %s",
          requested_compositor, compositors[0].c_str ());
      selected_compositor = compositors[0];
    } else {
      selected_compositor = std::string (requested_compositor);
    }
  } else {
    selected_compositor = compositors[0];
  }

  selected_sink = "autovideosink";
  if (requested_sink) {
    feature =
        gst_registry_find_feature (gst_registry_get (), requested_sink,
        GST_TYPE_ELEMENT_FACTORY);
    if (!feature) {
      gst_printerrln ("Requested %s is unavailable", requested_sink);
    } else {
      selected_sink = std::string (requested_sink);
      gst_object_unref (feature);
    }
  } else if (benchmark) {
    selected_sink = "fakevideosink";
  }

  config = gst_structure_new ("GstPipPlayerConfig",
      GST_PIP_PLAYER_VIDEO_DECODER,
      G_TYPE_STRING,
      selected_decoder.c_str (),
      GST_PIP_PLAYER_VIDEO_COMPOSITOR,
      G_TYPE_STRING,
      selected_compositor.c_str (),
      GST_PIP_PLAYER_VIDEO_SINK,
      G_TYPE_STRING,
      selected_sink.c_str (),
      GST_PIP_PLAYER_OVERLAY_SCALE_FACTOR,
      G_TYPE_DOUBLE,
      scale_factor,
      GST_PIP_PLAYER_TIMESTAMP_OFFSET,
      G_TYPE_INT64,
      timestamp_offset,
      GST_PIP_PLAYER_SEGMENT_START,
      G_TYPE_INT64,
      segment_start,
      GST_PIP_PLAYER_SEGMENT_STOP, G_TYPE_INT64, segment_stop, nullptr);

  if (benchmark) {
    /* disable sync for benchmark purpose */
    gst_structure_set (config, GST_PIP_PLAYER_SYNC, G_TYPE_BOOLEAN, FALSE,
        nullptr);
  }

  if (videosink_caps && g_str_has_prefix (videosink_caps, "video/x-raw")) {
    gst_structure_set (config,
        GST_PIP_PLAYER_VIDEO_SINK_CAPS_STR,
        G_TYPE_STRING, videosink_caps, nullptr);
  }

  config_str = gst_structure_to_string (config);
  gst_println ("Configuration: \n%s\n", config_str);
  g_free (config_str);

  bus = gst_bus_new ();
  player = gst_pip_player_new (config, bus);
  if (!gst_pip_player_set_uris (player, uris[0].c_str (), uris[1].c_str (),
          nullptr)) {
    gst_printerrln ("Failed to set uri");
    gst_object_unref (player);
    return 1;
  }

  app_data = g_new0 (AppData, 1);
  app_data->player = player;
  app_data->context = g_main_context_default ();
  app_data->loop = g_main_loop_new (app_data->context, FALSE);
  app_data->rate = 1;
  app_data->trick_mode = TRICK_MODE_NORMAL;
  app_data->target_state = GST_STATE_PLAYING;
  app_data->bus = bus;
  app_data->benchmark = benchmark;

  gst_bus_add_watch (bus, (GstBusFunc) bus_handler, app_data);

  before = gst_util_get_timestamp ();

  gst_println ("Press 'k' to see a list of keyboard shortcuts");

  gst_pip_player_play (app_data->player);
  if (!benchmark) {
    set_key_handler (app_data->context, (KeyInputCallback) key_input_callback,
        app_data);
  }

  timeout_source = g_timeout_source_new (100);
  g_source_set_callback (timeout_source, (GSourceFunc) query_position_cb,
      app_data, nullptr);
  g_source_attach (timeout_source, app_data->context);
  g_main_loop_run (app_data->loop);

  after = gst_util_get_timestamp ();
  diff = GST_CLOCK_DIFF (before, after);
  if (benchmark) {
    gst_println ("Execution ended after %" GST_TIME_FORMAT,
        GST_TIME_ARGS (diff));
  }

  if (!benchmark)
    unset_key_handler ();
  g_source_destroy (timeout_source);
  g_source_unref (timeout_source);

  gst_object_unref (app_data->player);
  g_main_loop_unref (app_data->loop);
  gst_object_unref (bus);

  g_free (app_data);

  g_free (requested_decoder);
  g_free (requested_compositor);
  g_free (requested_sink);

  return 0;
}
