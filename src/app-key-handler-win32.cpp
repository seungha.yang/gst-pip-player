#include "app-key-handler.h"

#include <windows.h>

typedef struct _Win32KeyHandler
{
  GMainContext *context;
  GThread *thread;
  HANDLE cancellable;
  HANDLE console_handle;
  GMutex lock;
  gboolean closing;

  KeyInputCallback callback;
  gpointer user_data;
} Win32KeyHandler;

typedef struct
{
  KeyInputCallback callback;
  gpointer user_data;
  gchar value;
  gboolean is_ascii;
} KeyInputCallbackData;

static Win32KeyHandler *_handler = nullptr;

static gboolean
handler_source_func (KeyInputCallbackData * data)
{
  data->callback (data->value, data->is_ascii, data->user_data);

  return G_SOURCE_REMOVE;
}

static gpointer
handler_thread_func (Win32KeyHandler * handler)
{
  HANDLE handles[2];

  handles[0] = handler->cancellable;
  handles[1] = handler->console_handle;

  while (true) {
    DWORD ret = WaitForMultipleObjects (2, handles, FALSE, INFINITE);
    INPUT_RECORD buffer;
    DWORD num_read = 0;
    KeyInputCallbackData *data;

    if (ret == WAIT_FAILED) {
      gst_printerrln ("Wait failed");
      return nullptr;
    }

    g_mutex_lock (&handler->lock);
    if (handler->closing) {
      g_mutex_unlock (&handler->lock);

      return nullptr;
    }
    g_mutex_unlock (&handler->lock);

    if (!PeekConsoleInput (handler->console_handle, &buffer, 1, &num_read) ||
        num_read != 1)
      continue;

    ReadConsoleInput (handler->console_handle, &buffer, 1, &num_read);
    if (buffer.EventType != KEY_EVENT || !buffer.Event.KeyEvent.bKeyDown)
      continue;

    data = g_new0 (KeyInputCallbackData, 1);
    data->callback = handler->callback;
    data->user_data = handler->user_data;

    switch (buffer.Event.KeyEvent.wVirtualKeyCode) {
      case VK_UP:
        data->value = (gchar) KB_ARROW_UP;
        break;
      case VK_DOWN:
        data->value = (gchar) KB_ARROW_DOWN;
        break;
      case VK_LEFT:
        data->value = (gchar) KB_ARROW_LEFT;
        break;
      case VK_RIGHT:
        data->value = (gchar) KB_ARROW_RIGHT;
        break;
      default:
        data->value = buffer.Event.KeyEvent.uChar.AsciiChar;
        data->is_ascii = TRUE;
        break;
    }

    g_main_context_invoke_full (handler->context,
        G_PRIORITY_DEFAULT,
        (GSourceFunc) handler_source_func, data, (GDestroyNotify) g_free);
  }
}

void
set_key_handler (GMainContext * context,
    KeyInputCallback callback, gpointer user_data)
{
  if (_handler || !callback)
    return;

  _handler = g_new0 (Win32KeyHandler, 1);

  SECURITY_ATTRIBUTES attr;
  attr.nLength = sizeof (SECURITY_ATTRIBUTES);
  attr.lpSecurityDescriptor = nullptr;
  attr.bInheritHandle = FALSE;

  if (!context)
    context = g_main_context_default ();

  _handler->context = g_main_context_ref (context);
  _handler->cancellable = CreateEvent (&attr, TRUE, FALSE, nullptr);
  _handler->console_handle = GetStdHandle (STD_INPUT_HANDLE);
  _handler->callback = callback;
  _handler->user_data = user_data;
  g_mutex_init (&_handler->lock);
  _handler->thread =
      g_thread_new ("key-handler", (GThreadFunc) handler_thread_func, _handler);
}

void
unset_key_handler (void)
{
  if (!_handler)
    return;

  g_mutex_lock (&_handler->lock);
  _handler->closing = TRUE;
  g_mutex_unlock (&_handler->lock);

  SetEvent (_handler->cancellable);
  g_thread_join (_handler->thread);
  CloseHandle (_handler->cancellable);
  g_mutex_clear (&_handler->lock);
  g_main_context_unref (_handler->context);

  g_clear_pointer (&_handler, g_free);
}
